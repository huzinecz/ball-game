const gameSettings = {
	counterSeconds : 90, // Visszaszámlás ettől a másodpertől

	images: {
		background : "img/bg.jpg",
		ball : "img/ball3.png",
		point : "img/point.png",
	},
	
	moveSpeed : 2, // mozgás sebessége

	background: {
		moveLeft: 0.35,
		moveRight: 0.65
	},

	startPointYMultiplier: 0.8218232044198895, // a labda Y pozíciója úgy számolódik ki, hogy a magasságot megszorozzuk ezzel az értékkel 
	collisionApproximation: 50, // Közelítő az ütközésvizsgálathoz

	randomStarsCount: 100, // ennyi darab csillagot generál random a játék
};


var game = new Game();

function init(){
	game.init();
}

/**
 * Képeket tároló osztály
 */
var imageRepository = new function() {
	// képek definiálása
	this.background = new Image();
	this.ball = new Image();
	this.point = new Image();

	// Kép betöltések kezelése
	var numImages = 3;
	var numLoaded = 0;
	function imageLoaded() {
		numLoaded++;
		if (numLoaded === numImages) {
			window.init();
		}
	}
	this.background.onload = function() {
		imageLoaded();
	}
	this.ball.onload = function() {
		imageLoaded();
	}
	this.point.onload = function() {
		imageLoaded();
	}

	this.background.src = gameSettings.images.background;
	this.ball.src = gameSettings.images.ball;
	this.point.src = gameSettings.images.point;
}

/**
 * Absztrakt osztály 
 */
function Drawable() {	
	this.init = function(x, y) {
		this.x = x;
		this.y = y;
	}

	this.canvasWidth = 0;
	this.canvasHeight = 0;
	
	// absztrakt "rajzolás" függvény amit a származtatásnál felülírnak az elemek
	this.draw = function() {};
}


/**
 * Játék osztály
 */
function Game() {
	this.acquiredPoints = [];
	this.counter = null;
	this.name = ""; // felhasználó neve
	this.maxPoints = 0;
	this.intervals = { // intervalokat kezelő intervalsok tömbje
		up: null,
		down: null,
		counter: null
	};

	this.init = function() {
		if(this.name.length == 0){
			this.getName();
			return;
		}

		// Canvas elemek lekérdezése
		this.bgCanvas = document.getElementById('background');
		this.ballCanvas = document.getElementById('ball');
		this.pointsCanvas = document.getElementById('points');
		
		// Canvasok méretének beállítása attól függően, hogy mekkora a böngésző ablaka
		this.bgCanvas.height = this.ballCanvas.height = this.pointsCanvas.height = window.innerHeight;
		this.bgCanvas.width = this.ballCanvas.width = this.pointsCanvas.width = window.innerWidth-20;

		if (this.bgCanvas.getContext) {
			this.bgContext = this.bgCanvas.getContext('2d');
			this.ballContext = this.ballCanvas.getContext('2d');
			this.pointsContext = this.pointsCanvas.getContext('2d');

			
			Background.prototype.context = this.bgContext;
			Background.prototype.canvasWidth = this.bgCanvas.width;
			Background.prototype.canvasHeight = this.bgCanvas.height;

			Ball.prototype.context = this.ballContext;
			Ball.prototype.canvasWidth = this.ballCanvas.width;
			Ball.prototype.canvasHeight = this.ballCanvas.height;

			Points.prototype.context = this.pointsContext;
			Points.prototype.canvasWidth = this.pointsCanvas.width;
			Points.prototype.canvasHeight = this.pointsCanvas.height;

			// Háttér inicializálása
			this.background = new Background();
			this.background.init(0,0); 

			// Labda inicializálása
			this.ball = new Ball();
			this.ballStartX = 100;
			this.ballStartY = this.ballCanvas.height * gameSettings.startPointYMultiplier; // kiszámoljuk a labda pontos helyét
			this.ball.init(this.ballStartX, this.ballStartY, parseInt(this.ballCanvas.height/10), parseInt(this.ballCanvas.height/10));

			// Csillagok inicializálása
			this.points = new Points();
			this.points.init(this.ballStartX, this.ball.getMaxJump(), this.ballStartY, parseInt(this.ballCanvas.height/20), parseInt(this.ballCanvas.height/20));
			
			this.start();
		}

	};
	
	/** 
	 * Játék indítása
	 */
	this.start = function() {
		this.acquiredPoints = [];
		this.background.draw(0);
		this.ball.draw();
		this.points.draw();
		this.keyboardActions();
		this.printPoints(0);
		this.startCounter();
	};

	/**
	 * billentyűlenyomásokra vonatkozó listener
	 */
	this.keyboardActions = function(){
		document.removeEventListener('keydown', this.pressfunc, false);
		document.addEventListener('keydown', this.pressfunc);
	};

	this.pressfunc = function(e){
		e = parseInt(e.keyCode);
		// Majd ezt kell alkalmazni a keypresshez 
		// http://jsfiddle.net/mrtsherman/8NW2K/3/
		switch(e){
			case 37:
				game.ball.move('left');
				break;
			case 39:
				game.ball.move('right');
				break;
			case 38:
				game.ball.move('up');
				break;
		}
	}

	/**
	 * Név bekérés
	 */
	this.getName = function(){
		const that = this,  
				nameInp = document.getElementById('gamerName');
		var modal = document.getElementById('nameModal');

		document.getElementById("counterInModal").innerHTML = that.getCounterPrintData(gameSettings.counterSeconds);
		nameInp.value = this.name;

		var enterNameFunc = function(e){
			if(e.keyCode == 13){
				nameInp.removeEventListener('keydown', enterNameFunc);
				that.name = nameInp.value;
				document.getElementById('viewName').innerHTML = that.name;
				modal.style.display = "none";
				that.init();
			}
		};

		nameInp.addEventListener('keydown', enterNameFunc);
		modal.style.display = "block";
		nameInp.focus();
	};

	/**
	 * Pontszámok kiírása
	 */
	this.printPoints = function(points){
		document.getElementById('viewPoints').innerHTML = points;

		if(this.maxPoints < points){
			this.maxPoints = points;
			document.getElementById('maxPoints').innerHTML = this.maxPoints;
		}
	}

	/**
	 * Visszaszámlálás elindítása
	 */
	this.startCounter = function(){
		const that = this;
		if(this.intervals.counter){
			clearInterval(this.intervals.counter);
		}
		this.counter = gameSettings.counterSeconds;

		let printData = function(){
			document.getElementById('counter').innerHTML = that.getCounterPrintData(that.counter);
		};

		this.intervals.counter = setInterval(function(){
			that.counter--;
			printData();
			if(that.counter == 0){
				that.endGame();
			}
		}, 1000);

		printData();

	}

	this.getCounterPrintData = function(counter){
		return parseInt(counter / 60) + ':' + parseInt(counter % 60);
	}

	/**
	 * Játék végének a kezelése
	 */
	this.endGame = function(){
		document.removeEventListener('keydown', this.pressfunc, false);
		clearInterval(this.intervals.counter);
		var modal = document.getElementById('endModal');
		document.getElementById('viewPointsEndGame').innerHTML = this.acquiredPoints.length;

		let clickButtonEvent = function(){
			modal.style.display = "none";
			game.init();
		};
		
		document.getElementById('restart').removeEventListener("click", clickButtonEvent, false);
		document.getElementById('restart').addEventListener("click", clickButtonEvent);

		modal.style.display = "block";
	}

}


/**
 * Labdát kezelő osztály
 */
function Ball() {
	this.actualMove = 0;


	this.init = function(x, y, width, height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.actualDegres = 0;
		this.startX = x;
	}

	this.draw = function(degrees, move) {
		if(degrees){
			this.actualDegres += degrees;
			this.actualMove += move ? move : 0;
			this.xa = this.x+this.width/2+this.actualMove;

			if(this.xa < this.startX){
				this.xa = this.startX;
				this.actualMove = 0;
				this.actualDegres = 0;
			}

			this.context.clearRect(0,0,this.canvasWidth, this.canvasHeight);
			this.context.save();
			this.context.translate(this.xa, this.y+this.height/2);
			this.context.rotate(this.actualDegres*Math.PI/180);
			
			this.context.drawImage(imageRepository.ball,-this.width/2,-this.width/2, this.width, this.height);
			this.context.restore();
		}else{
			this.context.drawImage(imageRepository.ball, this.x, this.y, this.width, this.height);
		}

	};
	this.move = function(type) {
		const that = this;
		let speed = gameSettings.moveSpeed * 10;
		switch(type){
			case 'right' :  // mozgás jobbra
				if(game.ball.actualMove > game.ballCanvas.width * gameSettings.background.moveRight){ // ha elér egy bizonyos szintet, mozgatjuk a hátteret is
					game.background.draw(speed/2);
					game.points.draw(speed/2);
					game.ball.draw(speed);
				}else{
					game.ball.draw(speed, speed/2);
				}
				break;
			case 'left' :  // mozgás balra
				if(game.ball.actualMove < game.ballCanvas.width * gameSettings.background.moveLeft){ // ha eléri egy bizonyos szintet, mozgatjuk a hátteret is
					game.background.draw(-speed/2);
					if(game.background.x != 0){
						game.points.draw(-speed/2);
					}
					game.ball.draw(-1*speed, game.background.x == 0 ? -1*speed/2 : 0);
				}else{
					game.ball.draw(-1*speed, -1*speed/2);
				}
				break;
			case 'up':
				if(game.intervals.up != null || game.intervals.down != null){
					break;
				}

				const normalY = game.ball.y;
				const maxY = that.getMaxJump();
				let newGameY;

				game.intervals.up = setInterval(function(){
					if(game.ball.y <= maxY){
						clearInterval(game.intervals.up);
						game.intervals.up = null;
						game.intervals.down = setInterval(function(){
							newGameY = game.ball.y + 10 > normalY ? normalY : game.ball.y + 10;
							if(normalY === game.ball.y){
								clearInterval(game.intervals.down);
								game.intervals.down = null;
							}else{
								game.ball.y = newGameY;
								game.ball.ya = newGameY;
								game.ball.draw(3);
							}
						},1000/60);
					}else{
						game.ball.y -= 15;
						game.ball.draw(3);
						that.setPoint();
					}
				}, 1000/60);
				break;
		}

		this.setPoint();
	};

	//  hogy ha épp pontba ütközik akkor gyűjtsük be
	this.setPoint = function(){
		let point = game.points.getPoint(game.ball.xa + (-1*game.background.x), game.ball.y);
		if(point){
			game.acquiredPoints.push(point);
			game.points.draw();
			game.printPoints(game.acquiredPoints.length);
		}
	};

	this.getMaxJump = function(){
		return Math.floor( game.ball.y - game.bgCanvas.height*0.25 );
	};

}

/**
 * Pontszámokat kezelő osztály
 */
function Points() {
	this.pos = 0;
	this.randomStars = {};
	this.minY = null;
	this.maxY = null;
	this.minX = null;

	this.init = function(minX, minY, maxY, height, width){
		this.maxY = maxY;
		this.minX = minX;
		this.minY = minY;
		this.height = height;
		this.width = width;
		this.generateRandomStars(gameSettings.randomStarsCount);
	}

	this.draw = function(pos) {
		this.pos -= pos ? pos : 0;

		this.context.clearRect(0,0,this.canvasWidth, this.canvasHeight);

		const randomStarObjKeys = Object.keys(this.randomStars);

		for(let i = 0; i<randomStarObjKeys.length; i++){
			let star = this.randomStars[randomStarObjKeys[i]];
			this.context.drawImage(imageRepository.point, this.pos+star.x, star.y, this.height, this.width);
		}
	};


	this.generateRandomStars = function(c){
		this.randomStars = {};
		let x, y;

		for(let i = 0; i<c; i++){
			x = getRndInteger(this.minX, 10000), y = getRndInteger(parseInt(this.minY), parseInt(this.maxY));
			this.randomStars[x+'_'+y] = new OneStar(x, y);
		}
	};

	/**
	 * Pont visszaadása x és y koordináta alapján
	 * @param {*} x 
	 * @param {*} y 
	 */
	this.getPoint = function(x,y){
		x = parseInt(x)-29, y = parseInt(y);

		let xI = -gameSettings.collisionApproximation/2, startyI = yI = -gameSettings.collisionApproximation/2;
		let newX, newY, newKey = null;
		let point = null;

		while(xI <= gameSettings.collisionApproximation/2 && point == null){
			newX = x + xI;
			yI = startyI;
			while(yI <= gameSettings.collisionApproximation/2 && point == null){
				newY = y + yI;
				newKey = newX+'_'+newY;
				if(this.randomStars[newKey]){
					point = this.randomStars[newKey];
					delete this.randomStars[newKey];
					break;
				}
				yI++;
			}

			xI++;
		}

		return point;
	}

}



/**
 * Háttért kezelő osztály
 */
function Background() {
	
	this.draw = function(speed) {
		this.x -= speed;

		if(this.x > 0){ // visszafelé nem mehetünk
			this.x = 0;
		}

		this.context.drawImage(imageRepository.background, this.x, this.y, imageRepository.background.width, this.canvasHeight);
		
		// kétszer tesszük be a képet, hogy ha túlmegy rajta, folytatódjon
		this.context.drawImage(imageRepository.background, this.x+imageRepository.background.width, this.y, imageRepository.background.width, this.canvasHeight);

		// If the image scrolled off the screen, reset
		if (this.x <= -1*imageRepository.background.width)
			this.x = 0;
	};
}

/**
 * Random számot generáló függvény
 * @param {*} min 
 * @param {*} max 
 */
function getRndInteger(min, max) {
	return Math.floor(Math.random() * (max - min) ) + min;
}

/**
 * "Csillag" objektum
 * @param {*} x 
 * @param {*} y 
 */
function OneStar(x, y){

	this.x = x;
	this.y = y;

	return this;
}


Points.prototype = new Drawable();
Background.prototype = new Drawable();
Ball.prototype = new Drawable();

window.requestAnimFrame = (function(){
	return  window.requestAnimationFrame       || 
			window.webkitRequestAnimationFrame || 
			window.mozRequestAnimationFrame    || 
			window.oRequestAnimationFrame      || 
			window.msRequestAnimationFrame     || 
			function(/* function */ callback, /* DOMElement */ element){
				window.setTimeout(callback, 1000 / 60);
			};
})();

