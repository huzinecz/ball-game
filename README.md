# Web animáció - Saját animáció beadandó

## Feladat leírása
Készíts interaktív animációt webre.

Ez lehet játék, mese, történet, szimuláció, ... De interaktív.

A megvalósítás során egyaránt fontos az ötletesség és a kidolgozottság és a kód. Ne felejtsétek el a web-sw-ekkel kapcsolatos ergonómiai elveket sem!

Ehhez bármilyen eszközt használhatsz!

Az elkészült beadandókat az utolsó gyakorlaton legfeljebb 5 percben be kell mutatni a csoport előtt. A kapott észrevételeket érdemes komolyan átgondolni, és szükség esetén javítani az elkészült munkán, hogy jobb osztályzatot adhassunk.


## Futtatási információk
Javasolt böngésző: Google Chrome

De ugyanúgy működik ezekben a böngészőkben is: Opera, Mozzila Firefox

Az index.html fájlt megnyitva futtatható a játék.

## Fejlesztési információk
HTML5 canvas-t valamint javascriptet használtam a fejlesztés során
